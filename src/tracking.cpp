#include "opencv2/opencv.hpp"
#include "readdirectory.h" //list the files
#include "readfile.h" //read the laser files
#include "timer.h"

#include "TrackerKCF.h"

#include <iostream>

using namespace cv;
using namespace std;

int main(){
  Timer timer;
  KCF tracker;
  Rect roi;
  vector<string> images=read_directory("/home/kurnianggoro/dataset/Tracking/Miltrack/coke11/imgs/");
 
  //roi=Rect(138,120,38,59);//cliffbar
  roi=Rect(149,80,24,40);//coke11
  //roi=Rect(128,46,104,127);//girl
  //roi=Rect(142,62,62,98);//dollar

  bool needResize=false;
  if(sqrt(roi.width*roi.height)>100){
    roi.width/=2;roi.height/=2;
    roi.x/=2;
    roi.y/=2;
    needResize=true;
  }
  
  tracker.addObject(roi);
 
 
  Mat color,gray;
  for(unsigned i=0;i<images.size();i++){
    if(images[i].find(".png")==std::string::npos)
      continue;

    //cout<<images[i].c_str()<<" / "<<images.size()<<endl;
    color=imread(images[i].c_str());

    if(needResize)
      resize(color,color,Size(color.cols/2,color.rows/2));
    
    //convert to grayscale
    if (color.channels()==3){
      cvtColor(color,gray,COLOR_BGR2GRAY);
    }else gray=color;
    
    
  
    timer.start("track");
    tracker.track(gray,0.2,0.01,0.075);
    cout<<"timer: ";timer.plotcsv();
    timer.reset();
    tracker.draw(color);
    
    if(waitKey(1)==27)break; //quit on ESC button
//     while(waitKey(1)!=27){}
  }
   
}