#pragma once

#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include "writetofile.h"

using namespace cv;
using namespace std;

class KCF{
public:
  KCF();
  ~KCF(){};
  
  int addObject(Rect _roi);//add tracked object
  Rect track(Mat img);
  Rect track(Mat img, float sigma, float lambda, float interp_factor);
  void draw(Mat scene);
  void createHanningWindow(OutputArray _dst, cv::Size winSize, int type);
  void getSubWindow(Mat img, Rect roi, Mat& patch);
  
private:
  float sigma,lambda,interp_factor,output_sigma_factor,output_sigma;
  Mat object;
  Rect roi,roi_ori;
  Mat scene,gray;
  Mat new_alphaf, alphaf,k,y,yf,z,new_z,response,kf;
  
  unsigned frame;
  
  bool templateIsAvailable;
  Mat hann; //hann window filter
  
  Mat X;//training patch
  
  void shiftRows(Mat& mat); 
  void shiftRows(Mat& mat,int n);
  void shiftCols(Mat& mat, int n);
  void denseGaussKernel(float sigma, Mat x, Mat y, Mat & k);
  void calcResponse(Mat alphaf, Mat k, Mat & response);
  void inline fft2(Mat src, Mat & dest);
  void inline ifft2(Mat src, Mat & dest);  
  
};

KCF::KCF():
  frame(0),
  templateIsAvailable(false),
  sigma(0.2),
  lambda(0.01),
  interp_factor(0.075),
  output_sigma_factor(1.0/16.0)
{
  
}

int KCF::addObject(Rect _roi){
  Mat Kx,Ky;
  roi=_roi;
  roi_ori=roi;
    
  output_sigma=sqrt(roi.width*roi.height)*output_sigma_factor;
  output_sigma=-0.5/(output_sigma*output_sigma);
  
  roi.x-=roi.width/2;
  roi.y-=roi.height/2+1;
  roi.width*=2;
  roi.height*=2;
  
  templateIsAvailable=true;
  createHanningWindow(hann, roi.size(), CV_32F);
  
  //gauss response
  y=Mat::zeros(roi.height,roi.width,CV_32F);
  for(int i=0;i<roi.height;i++){
    for(int j=0;j<roi.width;j++){
      y.at<float>(i,j)=(i-roi.height/2+1)*(i-roi.height/2+1)+(j-roi.width/2+1)*(j-roi.width/2+1);
    }
  }

  y*=(double)output_sigma;
  cv::exp(y,y);

  fft2(y,yf);
  
  //saveMatFloat(y,"Gy.csv");
  //saveMat(yf,"Gyf.csv");

}

Rect KCF::track(Mat img){
  return track(img, sigma, lambda, interp_factor);
}

Rect KCF::track(Mat img, float sigma, float lambda, float interp_factor){
  double minVal, maxVal;
  Point minLoc,maxLoc;
  
  //TODO: make this checking better
  if(!templateIsAvailable || img.channels()>1)
    return roi;
  
      
  //preprocess patch
  getSubWindow(img,roi, X);

  if(frame>0){
    denseGaussKernel(sigma,X,z,k);
    if(frame==1){
//       saveMatFloat(X,"rx.csv");
//       saveMatFloat(z,"rz.csv");
//       saveMatFloat(k,"rk.csv");
    }
    calcResponse(alphaf,k,response);
    minMaxLoc( response, &minVal, &maxVal, &minLoc, &maxLoc );
    roi.x+=(maxLoc.x-roi.width/2+1);roi.y+=(maxLoc.y-roi.height/2+1);
    //cout<<maxLoc.x<<" "<<maxLoc.y<<" "<<(maxLoc.x-roi.width/2)<<" "<<(maxLoc.y-roi.height/2)<<" "<<minVal<<" "<<maxVal<<endl;
  }
  
  getSubWindow(img,roi, X);
  
  denseGaussKernel(sigma,X,X,k);
//   saveMatFloat(k,"Gk.csv");
  fft2(k,kf);
//   saveMat(kf,"Gkf.csv");
  kf=kf+lambda;
//   saveMat(kf,"Gkflambda.csv");
  new_alphaf=Mat_<std::complex<float> >(yf.rows, yf.cols); 
  for(int i=0;i<yf.rows;i++){
    for(int j=0;j<yf.cols;j++){
      new_alphaf.at<std::complex<float> >(i,j)=std::complex<float>(yf.at<Vec2f>(i,j)[0],yf.at<Vec2f>(i,j)[1])/std::complex<float>(kf.at<Vec2f>(i,j)[0],kf.at<Vec2f>(i,j)[1]);
    }
  }
   
// saveMat(new_alphaf,"GnewAf.csv");
  new_z=X.clone();
  
  if(frame==0){
    alphaf=new_alphaf.clone();
    z=X;
  }else{
    alphaf=(1.0-interp_factor)*alphaf+interp_factor*new_alphaf;
    z=(1.0-interp_factor)*z+interp_factor*new_z;
  }  
   
// saveMat(alphaf,"Galphaf.csv");
  frame++;
  return roi;
}

void KCF::draw(Mat scene){
  roi_ori.x=roi.x+roi_ori.width/2;
  roi_ori.y=roi.y+roi_ori.height/2;
  //add rectangle
  rectangle(
    scene,roi,        
    Scalar(0,255,0),
    1,4
  );
    rectangle(
    scene,roi_ori,        
    Scalar(0,255,0),
    4,4
  );
  
  //draw
  imshow("tracking", scene);
}

void KCF::createHanningWindow(OutputArray _dst, cv::Size winSize, int type)
{
    CV_Assert( type == CV_32FC1 || type == CV_64FC1 );

    _dst.create(winSize, type);
    Mat dst = _dst.getMat();

    int rows = dst.rows, cols = dst.cols;

    AutoBuffer<double> _wc(cols);
    double * const wc = (double *)_wc;

    double coeff0 = 2.0 * CV_PI / (double)(cols - 1), coeff1 = 2.0f * CV_PI / (double)(rows - 1);
    for(int j = 0; j < cols; j++)
        wc[j] = 0.5 * (1.0 - cos(coeff0 * j));

    if(dst.depth() == CV_32F)
    {
        for(int i = 0; i < rows; i++)
        {
            float* dstData = dst.ptr<float>(i);
            double wr = 0.5 * (1.0 - cos(coeff1 * i));
            for(int j = 0; j < cols; j++)
                dstData[j] = (float)(wr * wc[j]);
        }
    }
    else
    {
        for(int i = 0; i < rows; i++)
        {
            double* dstData = dst.ptr<double>(i);
            double wr = 0.5 * (1.0 - cos(coeff1 * i));
            for(int j = 0; j < cols; j++)
                dstData[j] = wr * wc[j];
        }
    }

    // perform batch sqrt for SSE performance gains
    //cv::sqrt(dst, dst); //matlab do not use the square rooted version
}

void KCF::denseGaussKernel(float sigma, Mat x, Mat y, Mat & k){
  Mat xf, yf, xyf,xy;
  double normX, normY;
  
  fft2(x,xf);
  fft2(y,yf);
  normX=norm(x);
  normX*=normX;
  normY=norm(y);
  normY*=normY;
  
  mulSpectrums(xf,yf,xyf,0,true);
    
  ifft2(xyf,xyf);
  shiftRows(xyf, x.rows/2);
  shiftCols(xyf,x.cols/2);

  //(xx + yy - 2 * xy) / numel(x)
  xy=(normX+normY-2*xyf)/(x.rows*x.cols);


  threshold(xy,xy,0.0,0.0,THRESH_TOZERO);//max(0, (xx + yy - 2 * xy) / numel(x))

  float sig=-1.0/(sigma*sigma);
  xy=sig*xy;
  exp(xy,k);

}

//http://stackoverflow.com/questions/10420454/shift-like-matlab-function-rows-or-columns-of-a-matrix-in-opencv
//circular shift one row from up to down
void KCF::shiftRows(Mat& mat) {

    Mat temp;
    Mat m;
    int k = (mat.rows-1);
    mat.row(k).copyTo(temp);
    for(; k > 0 ; k-- ) {
        m = mat.row(k);
        mat.row(k-1).copyTo(m);
    }
    m = mat.row(0);
    temp.copyTo(m);

}

//circular shift n rows from up to down if n > 0, -n rows from down to up if n < 0
void KCF::shiftRows(Mat& mat,int n) {

    if( n < 0 ) {

        n = -n;
        flip(mat,mat,0);
        for(int k=0; k < n;k++) {
            shiftRows(mat);
        }
        flip(mat,mat,0);

    } else {

        for(int k=0; k < n;k++) {
            shiftRows(mat);
        }
    }

}

//circular shift n columns from left to right if n > 0, -n columns from right to left if n < 0
void KCF::shiftCols(Mat& mat, int n) {

    if(n < 0){

        n = -n;
        flip(mat,mat,1);
        transpose(mat,mat);
        shiftRows(mat,n);
        transpose(mat,mat);
        flip(mat,mat,1);

    } else {

        transpose(mat,mat);
        shiftRows(mat,n);
        transpose(mat,mat);
    }
}

void KCF::calcResponse(Mat alphaf, Mat k, Mat & response){
  //alpha f--> 2channels ; k --> 1 channel; 
  Mat kf;
  fft2(k,kf);
Mat spec;
  mulSpectrums(alphaf,kf,spec,0,false);
  ifft2(spec,response);


}

void inline KCF::fft2(Mat src, Mat & dest){
  Mat planes[] = {Mat_<float>(src), Mat::zeros(src.size(), CV_32F)};
  merge(planes, 2, dest); 
  dft(dest,dest,DFT_COMPLEX_OUTPUT);
}

void inline KCF::ifft2(Mat src, Mat & dest){
  idft(src,dest,DFT_SCALE+DFT_REAL_OUTPUT);
}

void KCF::getSubWindow(Mat img, Rect roi, Mat& patch){

  Rect region=roi;
  if(roi.x<0){region.x=0;region.width+=roi.x;}
  if(roi.y<0){region.y=0;region.height+=roi.y;}
  if(roi.x+roi.width>img.cols)region.width=img.cols-roi.x;
  if(roi.y+roi.height>img.rows)region.height=img.rows-roi.y;
  if(region.width>img.cols)region.width=img.cols;
  if(region.height>img.rows)region.height=img.rows;
    
  patch=img(region).clone();
  
  int addTop,addBottom, addLeft, addRight;
  addTop=region.y-roi.y;
  addBottom=(roi.height+roi.y>img.rows?roi.height+roi.y-img.rows:0);
  addLeft=region.x-roi.x;
  addRight=(roi.width+roi.x>img.cols?roi.width+roi.x-img.cols:0);
  
  copyMakeBorder(patch,patch,addTop,addBottom,addLeft,addRight,BORDER_REPLICATE);

  patch.convertTo(patch,CV_32F);

  patch=patch/255.0-0.5; // normalize to range -0.5 .. 0.5
  patch=patch.mul(hann);

}