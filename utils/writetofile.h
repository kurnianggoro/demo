#pragma once
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;

void saveMat(Mat &image, const char * filename){
    FILE *stream;
    stream=fopen(filename,"w");

    if(stream!=NULL){
      for(unsigned k=0;k<image.channels();k++){
        for(unsigned i=0;i<image.rows;i++){
	  for(unsigned j=0;j<image.cols;j++){
            if(j!=0)fprintf(stream,",");
            fprintf(stream,"%f",image.at<Vec2f>(i,j)[k]);
	  }
	  fprintf(stream,"\n");
        }
      }
    }
}

void saveMatChar(Mat &image, const char * filename){
    FILE *stream;
    stream=fopen(filename,"w");

    if(stream!=NULL){
      for(unsigned k=0;k<image.channels();k++){
        for(unsigned i=0;i<image.rows;i++){
	  for(unsigned j=0;j<image.cols;j++){
            if(j!=0)fprintf(stream,",");
            fprintf(stream,"%i",image.at<uchar>(i,j));
	  }
	  fprintf(stream,"\n");
        }
      }
    }
}

void saveMatFloat(Mat &image, const char * filename){
    FILE *stream;
    stream=fopen(filename,"w");

    if(stream!=NULL){
      for(unsigned k=0;k<image.channels();k++){
        for(unsigned i=0;i<image.rows;i++){
	  for(unsigned j=0;j<image.cols;j++){
            if(j!=0)fprintf(stream,",");
            fprintf(stream,"%f",image.at<float>(i,j));
	  }
	  fprintf(stream,"\n");
        }
      }
    }
}