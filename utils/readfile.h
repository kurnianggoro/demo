#ifndef KREADFILE_H_
#define KREADFILE_H_

#include <sstream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iostream>
//#include <typeinfo>
using namespace std;

template <class T>
void readDataFromFile(const char * filename,T * source,int row,int col, char delim=' '){
    ifstream inFile (filename);
    string line;
    int linenum = 0;
	int x=0;
	int y=0;
	T* input=(T*)(source);
	//printf("%s\n",typeid(source).name());
    while (getline (inFile, line))
    {


        istringstream linestream(line);
        string item;

        while (getline (linestream, item, delim))
        {

			input[x*col+y]=(T)atof(item.c_str());
			//cout<<item<<" ";
			y++;
			if(x*col+y>row*col)break;
        }
		x++;
		if(x*col+y>row*col)break;
    }
}
#endif 